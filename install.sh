#!/bin/bash

THIS_DIR="$(dirname $(realpath $0))"

# Default values
ASSUME_YES=0
DEST_DIR="$HOME"
DRY_RUN=0

dotfiles=(.bashrc .bash_aliases .condarc .zshrc .tmux.conf .inputrc .gitconfig .nanorc)

function print_usage {
    if [ -n "$1" ]; then
        echo "$1"
        echo
    fi
    echo "Usage: install.sh [OPTION]... [DEST_DIR]"
    echo
    echo "DEST_DIR:         The destination dir. Defaults to \$HOME"
    echo
    echo "Options:"
    echo "  -y, --yes       Do not prompt and assume yes to all"
    echo "  -d, --dry-run   Do not perform any actual file movement of copy"
    echo "  -h, --help      Prints this message"
}

function parse_cmdline {
    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -y|--yes)
        ASSUME_YES=1
        shift
        ;;
        -d|--dry-run)
        DRY_RUN=1
        shift
        ;;
        -h|--help)
        print_usage
        exit 0
        ;;
        -*) # unknown option
        print_usage "Unrecognized option: $key"
        exit 1
        ;;
        *)
        if [ $# -gt 1 ]; then
            print_usage "Too many arguments"
            exit 1
        fi
        DEST_DIR="$(realpath $1)"
        shift
        ;;
    esac
    done
}

function prompt {
    if [[ $ASSUME_YES -ne 0 ]]; then
        # Assuming yes
        return 0
    fi

    while true; do
        read -p "Do you want to overwrite existing $1? (A backup copy will be created anyway) (y|[n]) " yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            "" ) return 1;;
            * ) echo "Please answer y or n";;
        esac
    done
}


function do_install_item {
    local src=$1
    local dest=$2

    if [ -h $dest ] || [ -f $dest ] || [ -d $dest ]; then
        prompt $dest
        if [[ $? -ne 0 ]]; then
            echo "Skipping existing item: $dest"
            return
        fi
        echo "Backing up $dest as $dest.backup"
        [[ $DRY_RUN -eq 0 ]] && mv -v  $dest "$dest.backup"
    fi

    echo "Creating link $dest -> $src"
    [[ $DRY_RUN -eq 0 ]] && ln -svf $src $dest
}

function do_install {
    echo "DEST_DIR: $DEST_DIR"
    echo "ASSUME_YES: $ASSUME_YES"
    echo "DRY_RUN: $DRY_RUN"
    echo
    for file in ${dotfiles[@]}
    do
        do_install_item "$THIS_DIR/$file" "$DEST_DIR/$file"
    done

    echo "Installing nvim config"
    [[ $DRY_RUN -eq 0 ]] && mkdir -pv "${DEST_DIR}/.config"
    do_install_item "$THIS_DIR/nvim" "$DEST_DIR/.config/nvim"
    echo "Done!"
}

parse_cmdline $@
do_install
