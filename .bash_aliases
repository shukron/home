# User-defined aliases for bash/zsh
alias ..='cd ..'
alias cd..='cd ..'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
