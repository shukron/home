# Home

A collection of "dotfiles" like .bashrc, .condarc and other configurations that usually resides in the home directory

## What's in the box
1. `.bashrc`
2. `.zshrc` with `agnoster` theme for Oh My Zsh
3. `.inputrc` - for up/down history lookup with prefix
4. `.bash_aliases` - for user-defined aliases
5. `.condarc` with conda-forge channel and disable PS1 change
(done in `.[bash/zsh]rc`)
6. `.tmux.conf` - with `CTRL-A` as the command prefix and some themeing
7. `nvim` configuration

## Installation
Clone this repo to a location of your choice.
Put it in the same file system as your root (e.g not external drive)

```sh
git clone https://gitlab.com/shukron/home.git <clone-path>
cd <clone-path>
chmod u+x install.sh
./install.sh
```

The script will create sym-links on the selected directory, pointing to the
repository so that pulling updates later will be easy as `git pull`.

Existing files will be backed-up.

### Detailed Usage
```sh
./install.sh --help

Usage: install.sh [OPTION]... [DEST_DIR]

DEST_DIR:         The destination dir. Defaults to $HOME

Options:
  -y, --yes       Do not prompt and assume yes to all
  -d, --dry-run   Do not perform any actual file movement of copy
  -h, --help      Prints this message
```

## Updates
Pulling updates is as easy as `git pull`:
```sh
cd <clone-path>
git pull
```